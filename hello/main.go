package main

import (
	"fmt"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Println("Received body: ", request.Body)
    strB, _ := json.Marshal("Hello from GoLang!")
    fmt.Println(string(strB))
	return events.APIGatewayProxyResponse{Body: string(strB), StatusCode: 200}, nil
}

func main() {
	lambda.Start(Handler)
}
